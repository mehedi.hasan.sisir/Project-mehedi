<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\assign\Assign;
$objLoginUser = new user_login();
$objLoginUser -> login_check();
$objSearch = new Assign();
if($_SERVER['REQUEST_METHOD'] == 'GET'){

include_once '../header.php';

?>
</div>
    </div>
    <!-- Content area -->
    <div class="content">
<?php

$allSearchData = $objSearch -> search($_GET);

if(isset($allSearchData) && !empty($allSearchData)){
?>

<div class="panel">
	<h4 class="text-center">Search result is showing for "<?php echo '<b>'.$_GET['searchText'].'</b>'; ?>" keyword below</h4>
</div>

<div class="row panel">
        <div class="col-md-12">
           <div class="panel-body">
        <table class="table datatable-basic table-bordered table-striped table-hover dataTable no-footer">
        <?php

        	if($_GET['search_field'] == 'full'){

        ?>
        <!--start the full search body -->
            <thead>
                <tr>
                    <th class="col-md-1"><strong>Course<br>Name</strong></th>
                    <th class="col-md-1"><strong>Batch</strong></th>
                    <th class="col-md-1"><strong>Lead<br>Trainer</strong></th>
                    <th class="col-md-1"><strong>Lab<br>Num</strong></th>
                    <th class="col-md-2 text-center"><strong>Date</strong></th>
                    <th class="col-md-3"><strong>Day</strong></th>
                    <th class="col-md-3 text-center"><strong>Action</strong></th>
                    
                </tr>
            </thead>
            <tbody>
    <?php
    
    foreach ($allSearchData as $singleSearch) {

    ?>
    <tr>
        <td><?php echo $singleSearch['title']; ?></td>
        
        <td><?php echo $singleSearch['batch_no']; ?></td>
        <td><?php echo $singleSearch['full_name']; ?></td>
        <td><?php echo $singleSearch['lab_no']; ?></td>
        
        <td class ="text-size-small text-center">
        <?php echo date('M d Y',strtotime($singleSearch['start_date'])); ?>
         - 
        <?php echo date('M d Y',strtotime($singleSearch['ending_date'])); ?>
        <br>
        <?php echo date('h:i A',strtotime($singleSearch["start_time"])).'-'.date('h:i A',strtotime($singleSearch["ending_time"])); ?>

        </td>
        <td>
        <?php
            if($singleSearch['day']== 'day1'){
            echo 'Sat-Mon-Wed';}
            if($singleSearch['day']== 'day2'){
            echo 'Sun-Tue-Thu';}
            if($singleSearch['day']== 'day3'){
            echo 'Friday';}
        ?></td>
        <td class="text-center">
            <a class="btn bg-teal-800 btn-icon btn-xs" type="button" href="single_session.php?id=<?php echo $singleSearch['id']; ?>">
                <i class="icon-enlarge6"></i>
            </a>
            <?php
            	if($singleSearch['is_running'] == 1){
            ?>
            <a class="btn bg-teal-800 btn-icon btn-xs" type="button" href="edit_assign.php?id=<?php echo $singleSearch['id']; ?>">
                <i class="icon-pencil7"></i>
            </a>
            <?php
                if($_SESSION['logged']['is_admin'] == 1){
            ?>
            <a class="btn bg-teal-800 btn-icon btn-xs" type="button" href="trash.php?id=<?php echo $singleSearch['id']; ?>" onclick="return confirm('Are you sure you want to disable this course session?');"><i class="icon-close2"></i>
            </a>
            
            <?php
            	}// if logged as admin
            ?>
            <?php
            	}else{ // is is running == 1
            ?>
            <a class="btn border-teal-800 text-teal-800 btn-flat btn-icon btn-xs" type="button" href="restore.php?id=<?php echo $singleAssign['id']; ?>">
                <i class="icon-reset"></i>
            </a>
            <?php
                if($_SESSION['logged']['is_admin'] == 1){
            ?>
            <a class="btn border-teal-800 text-teal-800 btn-flat btn-icon btn-xs" type="button" href="delete.php?id=<?php echo $singleAssign['id']; ?>" onclick="return confirm('Are you sure you want to parmently delete this  session?');"><i class="icon-trash"></i>
            </a>
            <label class="validation-error-label text-danger text-left"><b>This session is disabled</b></label>

            <?php
            }// if logged as admin

            	} // else is running == 1
            ?>
            
        </td>


    </tr>
        <?php

            	}// foreach
            }elseif($_GET['search_field'] == 'course'){ // if get search_field == fill
        ?>
         <!--start the Course search body -->
        	<thead>
                <tr>
					<th class="text-center col-md-3">Course Name</th>
					<th class="text-center col-md-2">Duration</th>
					<th class="col-md-2">Type</th>
					<th class="col-md-1">Course<br>Fee (tk)</th>
					<th class="col-md-1">Offered</th>
					<th class="col-md-3">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if(isset($allSearchData) && !empty($allSearchData)){
					foreach ($allSearchData as $singleSearch) {
				
				?>
				<tr>
					<td><?php echo $singleSearch['title']; ?></td>
					<td><?php
							
							if ($singleSearch['duration'] == '1_month') {
							echo "1 Month Long";
							}
							if ($singleSearch['duration'] == '2_months') {
							echo "2 Months Long";
							}
							if ($singleSearch['duration'] == '3_months') {
							echo "Maximum 3 Months";
							}
					?></td>
					<?php
					if ($singleSearch['course_type'] == 'free') {
					echo '<td><b><span class="label label-flat border-danger text-danger-600"><b>FREE Course</b></span></b></td>';
					} else {
					echo '<td>Paid Course</td>';
					}
					?>
					<td>
						<?php
						if ($singleSearch['course_fee'] == '0') {
						echo'<p>Not Applicable</p>';
						} else {
						echo $singleSearch['course_fee'];
						}
						?>
					</td>
					<td class ="text-center">
						<?php
						if ($singleSearch['is_offer'] == 1) {
						echo '<br><span class="badge bg-indigo">Offered</span>';
						}else{
							echo "Not offered";
						}
						?>
					</td>
					<td class="text-center">
						<div class="btn-group">
							<a href="../courses/single_course.php?id=<?php echo $singleSearch['unique_id'];?>" class="label bg-slate-600 label-icon"><i class="icon-enlarge"></i>
							</a>
							</a>
							<?php
				            	if($singleSearch['is_delete'] == 1){
				            ?>
							<a href="../courses/restore_course.php?id=<?php echo $singleCourses['unique_id'];?>" class="label bg-slate-600 label-icon"><i class="icon-reset"></i>
							</a>
							<?php 
						    	if($_SESSION['logged']['is_admin'] == 1){
						    ?>
							<a href="../courses/delete_course_parmanent.php?id=<?php echo $singleCourses['unique_id'];?>" class="label bg-slate-600 label-icon"  onclick = "return confirm('Are you sure to parmanent delete this Course?')"><i class="icon-trash"></i>
							</a>
							<label class="validation-error-label text-danger text-left"><b>This Course is now disabled</b></label>

							<?php
								}// session logged as admin
							}else{ // if is_delete == 1
							?>

							<a href="../courses/edit_course.php?id=<?php echo $singleSearch['unique_id'];?>" class="label bg-slate-600 label-icon"><i class="icon-pencil7"></i>
							<?php
                                    if($_SESSION['logged']['is_admin'] == 1){
                                ?>
							<a href="../courses/delete_courses.php?id=<?php echo $singleSearch['unique_id'];?>" class="label bg-slate-600 label-icon"  onclick = "return confirm('Are you sure to disable this Course?')"><i class="icon-close2"></i>
							</a>
							<?php
				            }// session logged as admin
							?>

							<?php
							}// else is_delete == 1
							?>
						</div>
					</td>
				</tr>
				<?php
					}// foreach
				}// if $allLabInfo not empty

				}elseif($_GET['search_field'] == 'trainer'){ // if get search_field == fill
        ?>
         <!--start the Trainer search body -->
        	<thead>
                <tr>
					<th class="text-center col-md-3">Full Name</th>
					<th class="text-center col-md-2">trainer_status</th>
					<th class="col-md-1">Team</th>
					<th class="col-md-1">Course Name</th>
					<th class="col-md-1">Phone</th>
					<th class="col-md-4">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if(isset($allSearchData) && !empty($allSearchData)){
					foreach ($allSearchData as $singleSearch) {
				
				?>
				<tr>
					<td><?php echo $singleSearch['full_name']; ?></td>
					<td>
					<?php

							if ($singleSearch['trainer_status'] == 'lead_trainer') {
							echo "Lead Trainer";
							}
							if ($singleSearch['trainer_status'] == 'assist_trainer') {
							echo "Assistant Trainer";
							}
							if ($singleSearch['trainer_status'] == 'lab_assist') {
							echo "Lab Assistant";
							}
					?>
					</td>
					<td><?php echo $singleSearch['team']; ?></td>
					<td><?php echo $singleSearch['title']; ?></td>
					<td><?php echo $singleSearch['phone']; ?></td>

					<td class="text-center">
						<div class="btn-group">
							<a href="../trainers/single_trainer.php?id=<?php echo $singleSearch['unique_id'];?>" class="label bg-slate-600 label-icon"><i class="icon-enlarge"></i>
							</a>
							</a>
							<?php
				            	if($singleSearch['is_delete'] == 1){
				            ?>
							<a href="../trainers/restore_trainer.php?id=<?php echo $singleCourses['unique_id'];?>" class="label bg-slate-600 label-icon"><i class="icon-reset"></i>
							</a>
							<?php 
						    	if($_SESSION['logged']['is_admin'] == 1){
						    ?>
							<a href="../trainers/parment_delete.php?id=<?php echo $singleCourses['unique_id'];?>" class="label bg-slate-600 label-icon"  onclick = "return confirm('Are you sure to parmanent delete this Course?')"><i class="icon-trash"></i>
							</a>
							<label class="validation-error-label text-danger text-left">Trainer is disabled</label>

							<?php
								}// session logged as admin
							}else{ // if is_delete == 1
							?>

							<a href="../trainers/edit_trainer.php?id=<?php echo $singleSearch['unique_id'];?>" class="label bg-slate-600 label-icon"><i class="icon-pencil7"></i>
							<?php
                                    if($_SESSION['logged']['is_admin'] == 1){
                                ?>
							<a href="../trainers/delete_trainer.php?id=<?php echo $singleSearch['unique_id'];?>" class="label bg-slate-600 label-icon"  onclick = "return confirm('Are you sure to disable this Course?')"><i class="icon-close2"></i>
							</a>
							<?php
				            }// session logged as admin
							?>

							<?php
							}// else is_delete == 1
							?>
						</div>
					</td>
				</tr>
				<?php
					}// foreach
				}// if $allLabInfo not empty

            }
        }else{// if $allLabInfo not empty
        	echo '<div class="alert alert-danger alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button><h4 class="text-center">'.$_SESSION['searchError'].'</h4></div>';
        }
        ?>
            </tbody>
        </table>
       
        </div>
    </div>
    <?php 
    include_once 'footer.php';
	}else{
		header("location:list.php");
	}
    ?>