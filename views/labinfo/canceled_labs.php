<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\labinfo\labinfo;;
$objLoginUser = new user_login();
$objLoginUser -> login_check();

$objDisableLabs = new labinfo();
$allLabInfo = $objDisableLabs -> disable_labinfo_list();
$i = 1;
// echo "<pre>";
	// print_r($allLabInfo);
// echo "</pre>";
include_once '../header.php';
include_once 'menubar.php';
?>
<div class="panel panel-flat">
	<div class="panel-heading">
		<h3 class="panel-title text-center">All Lab info</h4>
	</div>
	<div class="panel-body">
		<table class="table datatable-basic datatable-responsive">
			<thead>
				<tr class="bg-grey-400">
					<th class="col-md-1">Lab Number</th>
					<th class="col-md-2">Assigned Course</th>
					<th class="col-md-2">Seat Capacity</th>
					<th class="col-md-2">PC Configuration</th>
					<th class="col-md-1">Operating<br>System</th>
					<th class="col-md-2">Trainer PC <br>Configuration</th>
					<th class="col-md-3">Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				if(isset($allLabInfo) && !empty($allLabInfo)){

					foreach ($allLabInfo as $key => $singleLabInfo) {
				
				?>
				<tr class="<?php 
					echo ($i % 2 == 0)?'alpha-brown':'alpha-grey';
					 $i++; ?>">

					<td class="text-center">
					<?php echo $singleLabInfo['lab_no']; 
						if($singleLabInfo['is_delete'] == 1){
							echo '<span class="label label-danger">Lab is offline</span>';
						}
					?>
					</td>
					<td><?php echo $singleLabInfo['title']; ?></td>
					<td><?php echo $singleLabInfo['seat_capacity']; ?> People</td>
					<td><?php
							if(isset($singleLabInfo['pc_configuration'])){
								$pc_config = unserialize($singleLabInfo['pc_configuration']);
							
							echo $pc_config[0]." PC, <br>".$pc_config[1]." Processor, <br>".$pc_config[2]." GB Ram <br>".$pc_config[3]." GB HDD";
							}
						?>
					</td>
					<td class = "text-center">
						<?php if($singleLabInfo['os'] == 'mac'){
							echo '<span class="label text-slate-800 label-rounded label-icon"><i class="icon-apple2"></i></span><br>Mac OS X';
						}
						elseif($singleLabInfo['os'] == 'window'){
							echo '<span class="label text-primary label-rounded label-icon"><i class="icon-windows8"></i></span><br>Windows 8+';
						}
						else{
							echo '<span class="label text-warning label-rounded label-icon"><i class="icon-tux"></i></span><br>Linux based OS';
						}
						?>
						
					</td>
					<td><?php echo $singleLabInfo['trainer_pc_configuration']; ?></td>
					<td class="text-center">
						<div class="btn-group">
							<a href="single_lab.php?id=<?php echo $singleLabInfo['id'];?>" class="label bg-grey-600 label-icon"><i class="icon-enlarge"></i>
							</a>
							<a href="restore_labs.php?id=<?php echo $singleLabInfo['id'];?>" class="label bg-grey-600 label-icon"><i class="icon-undo"></i>
							</a>
							<?php 
						    	if($_SESSION['logged']['is_admin'] == 1){
						    ?>
							<a href="delete_labs_parmenent.php?id=<?php echo $singleLabInfo['id'];?>" class="label bg-grey-600 label-icon" onclick = "return confirm('Are you sure to disable this Lab?')"><i class=" icon-trash-alt"></i>
							</a>
							<?php
								}
							?>
						</div>
					</td>
				</tr>
				<?php
					}// foreach
				}// if $allLabInfo not empty
				?>
			</tbody>
		</table>
	</div>
</div>
<!-- /basic datatable -->
<script type="text/javascript" src="../assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script type="text/javascript" src="../assets/js/pages/datatables_basic.js"></script>
<?php include_once 'footer.php' ?>