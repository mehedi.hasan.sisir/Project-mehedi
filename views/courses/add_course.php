<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\courses\courses;

$objLoginUser = new user_login();
$objLoginUser -> login_check();

$objAddCourses = new courses();



    function errMsg($data = ""){ // Show the Error session msg from class file
        if(isset($_SESSION["$data"])){
        ?>
            <label class="validation-error-label text-left">
        <?php
            if (!empty($_SESSION["$data"]) && isset($_SESSION["$data"])) {
                echo $_SESSION["$data"]."<br></label>";
                unset($_SESSION["$data"]);
            }
        }
    }

    function errMsgSuc($data = ""){ // Show the Error session msg in Green color
        if(isset($_SESSION["$data"])){
        ?>
            <label class="validation-error-label text-success text-left">
        <?php
            if (!empty($_SESSION["$data"]) && isset($_SESSION["$data"])) {
                echo $_SESSION["$data"]."<br></label>";
                unset($_SESSION["$data"]);
            }
        }
    }

    function session_value($data = ""){ // show the session msg in value prevent blank field.
        if(isset($_SESSION["$data"])){
            echo $_SESSION["$data"];
        }
    }

    function session_checked($value = "", $key = ""){ // to hold select point of dropdown 
        if(isset($_SESSION["$value"])){
            if($_SESSION["$value"] == "$key"){
                echo 'selected="selected"';
            }else{
                echo '';
            }
        }
    }

    function session_checked_radio($value = "", $key = ""){ // to hold checked of Radio 
        if(isset($_SESSION["$value"])){
            if($_SESSION["$value"] == "$key"){
                echo 'checked="checked"';
            }else{
                echo '';
            }
        }
    }
 
        

include_once '../header.php';
include_once 'menubar.php';
?>
<!-- Table -->
<?php $objAddCourses -> session_message('courseStoreSuccess'); ?>
<div class="panel-flat">
    <!-- Grid -->
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel registration-form">
                <div class="panel-body">
                    <div class="text-center">
                        <div class="icon-object border-teal text-teal"><i class="icon-graduation2"></i>
                        </div>
                        <h5 class="content-group-lg">Add New Course</h5>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Horizontal form -->
                            <div class="panel panel-flat">
                                <div class="panel-body">

                                <form action="add_course_action.php" class="form-horizontal course" method="POST" >
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Course Name</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control"  placeholder="Provide Course Name" name="title" value="<?php session_value('title');?>">
                                                    <?php errMsg('title_required');?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Course type</label>
                                                <div class="col-lg-9">
                                                    <label class="radio-inline">
		                                            <input type="radio" class="styled" name="course_type" value='paid' checked="checked"<?php session_checked_radio('course_type','paid')?> >
		                                            Paid Course
		                                        </label>

		                                        <label class="radio-inline">
		                                            <input type="radio" class="styled" name="course_type" value='free' id="free_course" <?php session_checked_radio('course_type','free')?> >
		                                            Free Course
		                                        </label>
		                                        <?php errMsg('course_type_required');?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Course Offer</label>
                                                <div class="col-lg-9">
	                                            	<label class="checkbox-inline">
													<input type="checkbox" name="is_offer" value="1" <?php session_checked_radio('is_offer','1') ?>>
													Offered Course
													</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">

                                        	<div class="form-group">
                                                <label class="control-label col-lg-3">Duration</label>
                                                <div class="col-lg-9">
                                                    <select class="bootstrap-select" data-width="100%" name="duration">
                                                    <option></option>
                                                    <option value="1_month"<?php session_checked('duration','1_month');?>>One Month</option>
                                                    <option value="2_months" <?php session_checked('duration','2_months');?>>Two Month</option>
                                                    <option value="3_months" <?php session_checked('duration','3_months');?>>Three Month</option>

                                                    </select>
                                                    <?php errMsg('duration_required');?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Course Price</label>
                                                <div class="col-lg-9">
                                                   <div class="input-group">
                                                   	 <input type="number" class="form-control"  placeholder="Provide Course Price" name="course_fee" value="<?php session_value('course_fee');?>" id="course_price">
                                                   	 <span class="input-group-addon">Taka</span>
                                                   </div>
                                                    <?php errMsg('course_fee_required');errMsg('course_type_change');?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Course Description</label>
                                                <div class="col-lg-9">
                                                   	<textarea rows="5" cols="5" class="form-control" name="description"
                                                  placeholder="Write description about that Courses"><?php session_value('description');?></textarea>
                                                    <?php errMsgsuc('description_required');?>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="text-right">
                                            <button class="btn bg-teal" type="submit">Create Course<i class="icon-arrow-right14 position-right"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <!-- /grid -->
    
    <script type="text/javascript" src="../assets/js/pages/uploader_bootstrap.js"></script>

    <script type="text/javascript" src="../assets/js/plugins/uploaders/fileinput.min.js"></script>
    <?php include_once 'footer.php';

?>