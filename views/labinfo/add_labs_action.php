<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\labinfo\labinfo;;

$objLoginUser = new user_login();
$objLoginUser -> login_check();

$objAddLabs = new labinfo();

$objAddLabs -> prepare($_POST);

$objAddLabs -> required_validation();

$objAddLabs -> add_labs();


?>

