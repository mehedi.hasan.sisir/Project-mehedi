<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\labinfo\labinfo;;
$objLoginUser = new user_login();
$objLoginUser -> login_check();

$objEditLabs = new labinfo();

$objEditLabs -> prepare($_GET);

$singleLabInfo = $objEditLabs -> single_labs();

$_SESSION['labinfo_id'] = $singleLabInfo['id'];

$course_id_title = $objEditLabs -> course_id_title();


    function errMsg($data = ""){ // Show the Error session msg from class file
        if(isset($_SESSION["$data"])){
        ?>
            <label class="validation-error-label text-left">
        <?php
            if (!empty($_SESSION["$data"]) && isset($_SESSION["$data"])) {
                echo $_SESSION["$data"]."<br></label>";
                unset($_SESSION["$data"]);
            }
        }
    }

    function errMsgSuc($data = ""){ // Show the Error session msg in Green color
        if(isset($_SESSION["$data"])){
        ?>
            <label class="validation-error-label text-success text-left">
        <?php
            if (!empty($_SESSION["$data"]) && isset($_SESSION["$data"])) {
                echo $_SESSION["$data"]."<br></label>";
                unset($_SESSION["$data"]);
            }
        }
    }

// if 
    function session_or_DB_value_update($session_data = "", $DB_data = ""){ 
    	global $singleLabInfo;
        if(isset($_SESSION["$session_data"]) && !empty($_SESSION["$session_data"])){
                echo $_SESSION["$session_data"];
                unset($_SESSION["$session_data"]);
	    }else{
	    	echo $singleLabInfo["$DB_data"];
	    }
	}

	function pc_config($session_data = "", $pc_array_index = ""){
		global $singleLabInfo;
		$pc_config = unserialize($singleLabInfo['pc_configuration']);

		if(isset($_SESSION["$session_data"]) && !empty($_SESSION["$session_data"])){
			echo $_SESSION["$session_data"];
			unset($_SESSION["$session_data"]);
		}else{
			echo $pc_config["$pc_array_index"];
		}
	}

	function pc_config_checked($value = "", $key = ""){ // to hold select point of dropdown 
		global $singleLabInfo;
		$pc_config = unserialize($singleLabInfo['pc_configuration']);

        if(isset($_SESSION["$value"])){
            if($_SESSION["$value"] == "$key"){
                echo 'selected="selected"';
            }elseif($pc_config[3] == "$key"){
                echo 'selected="selected"';
            }
        }
    }

    $internet = explode(" ", $singleLabInfo['internet_speed']);
    $singleLabInfo['internet_speed_data'] = $internet[0];
    $singleLabInfo['internet_speed_unite'] = $internet[1];


    function session_or_DB_checked($value = "", $key = ""){ // to hold select point of dropdown 
    	global $singleLabInfo;
        if(isset($_SESSION["$value"])){
            if($_SESSION["$value"] == "$key"){
                echo 'selected="selected"';
            }else{
                echo '';
            }
        }elseif($singleLabInfo["$value"] == "$key"){
                echo 'selected="selected"';
            }else{
                echo '';
            }
    }

    function session_data_exist($session_data = ""){
    	global $singleLabInfo;
    	if(isset($_SESSION["$session_data"]) && !empty($_SESSION["$session_data"])){
              if($_SESSION["$session_data"] != $singleLabInfo["$session_data"]){
              	 echo '<span class="text-warning"> *</span>';
              }
           }
    }


include_once '../header.php';
include_once 'menubar.php';
?>
<!-- Table -->
<?php $objEditLabs -> session_message('labAddSuccess'); ?>
<div class="panel-flat">
    <!-- Grid -->
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel registration-form">
                <div class="panel-body">
                    <div class="text-center">
                        <div class="icon-object border-brown text-brown"><i class="icon-display4"></i>
                        </div>
                        <h5 class="content-group-lg">Add New Lab</h5>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Horizontal form -->
                            <div class="panel panel-flat">
                                <div class="panel-body">

                                <form action="update_lab.php" class="form-horizontal labinfo" method="POST" id="labinfo">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Lab Number<?php session_data_exist('lab_no'); ?></label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control"  placeholder="Provide lab number" name="labnum" value="<?php session_or_DB_value_update('lab_no','lab_no');?>">
                                                    <?php errMsg('labnum_required');?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Select Course<?php session_data_exist('course_id');?></label>
                                                <div class="col-lg-9">
                                                    <select class="bootstrap-select" data-width="100%" name="course_id">
                                                        <option></option>

                                            <?php
                                                foreach($course_id_title as $course){
                                            ?>

                                            <option value="<?php echo $course['id'];?>" <?php session_or_DB_checked('course_id',$course['id']);?>><?php echo $course['title'];?>
                                            </option>

                                            <?php
                                                }
                                            ?>
                                                    </select>
                                                    <?php errMsg('course_id_required');?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Seat Capacity<?php session_data_exist('seat_capacity');?></label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" placeholder="Provide Seat Capacity" name="seat_capacity" value="<?php session_or_DB_value_update('seat_capacity','seat_capacity');?>">
                                                    <?php errMsg('seat_capacity_required');?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Projector Resulution<?php session_data_exist('projector_resolution');?></label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" placeholder="ex: 1200 X 800" name="projector_resolution" value="<?php session_or_DB_value_update('projector_resolution','projector_resolution');?>">
                                                    <?php errMsgSuc('projector_resolution_required');?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-lg-3">AC Status<?php session_data_exist('ac_status');?></label>
                                                <div class="col-lg-9">
                                                <select class="bootstrap-select" data-width="100%" name="ac_status">
                                                
                                                <option></option>

                                                <option value="0" <?php session_or_DB_checked('ac_status','0');?>>No AC
                                                </option>

                                                <option value="1" <?php session_or_DB_checked('ac_status','1');?>>One AC
                                                </option>

                                                <option value="2" <?php session_or_DB_checked('ac_status','2');?>>Two AC
                                                </option>

                                                <option value="3" <?php session_or_DB_checked('ac_status','3');?>>Three AC
                                                </option>

                                                </select>

                                    <?php errMsgSuc('ac_status_required');?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Table Capacity<?php session_data_exist('table_capacity');?></label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" placeholder="Provide Table Capacity" name="table_capacity" value="<?php session_or_DB_value_update('table_capacity','table_capacity');?>">
                                                    <?php errMsgSuc('table_capacity_required');?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Internet Speed<?php session_data_exist('internet_speed_data');?></label>
                                                <div class="col-lg-9">
                                                    <div class="col-md-6">
                                                    <input type="text" class="form-control" placeholder="ex: 512 Kbps/1 Mbps" name="internet_speed" value="<?php session_or_DB_value_update('internet_speed_data','internet_speed_data');?>">
                                                    <?php errMsgSuc('internet_speed_required');?>
                                                    </div>
                                                    <div class="col-md-6">
                                                    <select class="bootstrap-select" data-width="100%" name="internet_speed_unite">
                                                
                                                        <option value="kbps" <?php session_or_DB_checked('internet_speed_unite','kbps');?>>in Kbps
                                                        </option>

                                                        <option value="mbps" <?php session_or_DB_checked('internet_speed_unite','mbps');?>>in Mbps
                                                        </option>

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-lg-3">OS<?php session_data_exist('os');?></label>
                                                <div class="col-lg-9">
                                                <select class="bootstrap-select" data-width="100%" name="os">
                                                
                                                <option></option>

                                                <option value="window" <?php session_or_DB_checked('os','window');?>>Microsoft Windows
                                                </option>

                                                <option value="mac" <?php session_or_DB_checked('os','mac');?>>Mac OS X
                                                </option>

                                                <option value="linux" <?php session_or_DB_checked('os','linux');?>>Linux based OS
                                                </option>

                                                </select>

                                                <?php errMsg('os_required');?>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">PC Config
                                                <?php 
                                                	if(isset($_SESSION['brand_name']) || isset($_SESSION['pc_processor']) || isset($_SESSION['pc_ram']) || isset($_SESSION['pc_hdd']) || isset($_SESSION['ups'])){

                                                		echo '<span class="text-warning"> *</span>';
                                                	}         	
                                                ?></label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" placeholder="Brand name / Clone" name="brand_name" value="<?php pc_config('brand_name',0);?>">
                                                    <?php errMsg('brand_name_required');?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3"></label>
                                                <div class="col-lg-9">
                                                    <div class="col-lg-6" style="padding:0px">
                                                        <input type="text" class="form-control"  placeholder="Processor" name="pc_processor" value="<?php pc_config('pc_processor',1);?>">
                                                    <?php errMsg('pc_processor_required');?>
                                                    </div>
                                                    <div class="col-lg-6" style="padding:0px">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control"  placeholder="RAM" name="pc_ram" value="<?php pc_config('pc_ram',2);?>">
                                                        <span class="input-group-addon">GB</span>
                                                        </div>
                                                        <?php errMsg('pc_ram_required');?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-lg-3"></label>
                                                <div class="col-lg-9">
                                                    <div class="col-lg-6" style="padding:0px">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control"  placeholder="HardDisk" name="pc_hdd" value="<?php pc_config('pc_hdd',3);?>">
                                                        <span class="input-group-addon">GB</span>
                                                        </div>
                                                    <?php errMsg('pc_hdd_required');?>
                                                    </div>
                                                    <div class="col-lg-6 input-group" style="padding:0px">
                                                        <select class="bootstrap-select" data-width="100%" name="ups">
                                                
                                                        <option value="1" <?php pc_config_checked('ups','1');?>>UPS Powered
                                                        </option>

                                                        <option value="2" <?php pc_config_checked('ups','2');?>>UPS Absent
                                                        </option>

                                                        </select>

                                                        <?php errMsg('ups_required');?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Trainer PC<?php session_data_exist('trainer_pc_configuration');?></label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" placeholder="Trainers PC short description" name="trainer_pc_configuration" value="<?php session_or_DB_value_update('trainer_pc_configuration','trainer_pc_configuration');?>">
                                                    <?php
                                                        errMsg('trainer_pc_configuration_required');
                                                    ?>
                                                </div>
                                            </div>
                                            <input type="hidden" name="id" value="<?php $singleLabInfo['id'] ?>">
                                        </div>
                                        <div class="text-right">
                                            <button class="btn bg-brown" type="submit">Update Labinfo<i class="icon-arrow-right14 position-right"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <!-- /grid -->
    
    <script type="text/javascript" src="../assets/js/pages/uploader_bootstrap.js"></script>

    <script type="text/javascript" src="../assets/js/plugins/uploaders/fileinput.min.js"></script>

    <?php include_once 'footer.php' ?>