<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\courses\courses;
$objLoginUser = new user_login();
$objLoginUser -> login_check();

$objParmanentDeleteCourse = new courses();
$objParmanentDeleteCourse->prepare($_GET);

$objParmanentDeleteCourse->delete_course_parmanent();