<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\courses\courses;
$objLoginUser = new user_login();
$objLoginUser -> login_check();

$objSingleCourse = new courses();
$objSingleCourse->prepare($_POST);


$objSingleCourse -> validation();

$objSingleCourse->course_update();