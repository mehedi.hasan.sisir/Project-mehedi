
<?php
require_once '../../vendor/autoload.php';

use ProjectMehedi\user\user_registration\user_registration;

$objAddUser = new user_registration();

use ProjectMehedi\user\user_login\user_login;

$objLoginUser = new user_login();
$objLoginUser -> login_check();


include_once '../header.php';
include_once 'menubar.php';

?>
<!-- Table -->
<?php $objAddUser -> session_message('addSuccess'); ?>
<div class="panel-flat">
    <!-- Grid -->
    
    <div class="row">
        <div class="col-lg-offset-2 col-lg-8">
            <div class="panel registration-form">
                <div class="panel-body">
                    <div class="text-center">
                        <div class="icon-object border-teal-400 text-teal-400"><i class="icon-user-plus"></i>
                        </div>
                        <h5 class="content-group-lg">Create account</h5>
                        <!-- <form action="add_user_action.php" method="POST" enctype="multipart/form-data"> -->
                        <form action='add_user_action.php' method="POST" enctype="multipart/form-data">
                            <div class="form-group has-feedback">
                                <input type="text" placeholder="Choose username" class="form-control border-slate" name="username" value="<?php 
                                        $objAddUser->session_message('username'); ?>">
                                <div class="form-control-feedback">
                                    <i class="icon-user-plus text-muted"></i>
                                </div>
                                <?php
                                    if(isset($_SESSION['Uname_exists']) || isset($_SESSION['Uname_onlyText']) ||isset($_SESSION['Uname_charLength']) || isset($_SESSION['Uname_required']) ){
                                ?>
                                    <label class="validation-error-label text-left">
                                <?php 
                                    $objAddUser->session_message('Uname_exists');
                                    $objAddUser->session_message('Uname_onlyText');
                                    $objAddUser->session_message('Uname_charLength');
                                    $objAddUser->session_message('Uname_required');
                                ?>
                                    <br></label>

                                <?php
                                    }else{
                                ?>
                                    <span class="help-block text-right">Username is required</span>
                                <?php
                                    }
                                ?>    
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <input type="text" placeholder="First name" class="form-control border-slate" name="firstname" value="<?php 
                                        $objAddUser->session_message('firstname');
                                        ?>">
                                        <div class="form-control-feedback">
                                            <i class="icon-user-check text-muted"></i>
                                        </div>
                                <?php
                                    if(isset($_SESSION['firstname_onlyText']) || isset($_SESSION['firstname_charLimit'])){
                                ?>
                                    <label class="validation-error-label text-left">
                                <?php 
                                    $objAddUser->session_message('firstname_onlyText');
                                    $objAddUser->session_message('firstname_charLimit');
                                ?>
                                    <br></label>

                                <?php
                                    }elseif(isset($_SESSION['firstname_required'])){
                                ?>  
                                    <label class="validation-error-label text-success text-left">
                                <?php
                                        $objAddUser->session_message('firstname_required');
                                ?>
                                    <br></label>
                                <?php

                                    }else{
                                ?>
                                    <span class="help-block text-right">Firstname is Optional</span>
                                <?php
                                    }
                                ?>    
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <input type="text" placeholder="Last name" class="form-control border-slate" name="lastname" value="<?php 
                                        $objAddUser->session_message('lastname');
                                        ?>">
                                        <div class="form-control-feedback">
                                            <i class="icon-user-check text-muted"></i>
                                        </div>
                                <?php
                                    if(isset($_SESSION['lastname_onlyText']) || isset($_SESSION['lastname_charLimit'])){
                                ?>
                                    <label class="validation-error-label text-left">
                                <?php 
                                    $objAddUser->session_message('lastname_onlyText');
                                    $objAddUser->session_message('lastname_charLimit');
                                ?>
                                    <br></label>

                                <?php
                                    }elseif(isset($_SESSION['lastname_required'])){
                                ?>  
                                    <label class="validation-error-label text-success text-left">
                                <?php
                                        $objAddUser->session_message('lastname_required');
                                ?>
                                    <br></label>
                                <?php

                                    }else{
                                ?>
                                    <span class="help-block text-right">Lastname is Optional</span>
                                <?php
                                    }
                                ?>  
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <input type="password" placeholder="Create password" class="form-control border-slate"  name="password">
                                        <div class="form-control-feedback">
                                            <i class="icon-user-lock text-muted"></i>
                                        </div>
                                <?php
                                    if(isset($_SESSION['password_digiteRequired']) || isset($_SESSION['password_charRequired']) ||isset($_SESSION['password_charLength']) || isset($_SESSION['password_required']) ){
                                ?>
                                    <label class="validation-error-label text-left">
                                <?php 
                                    $objAddUser->session_message('password_digiteRequired');
                                    $objAddUser->session_message('password_charRequired');
                                    $objAddUser->session_message('password_charLength');
                                    $objAddUser->session_message('password_required');
                                ?>
                                    <br></label>

                                <?php
                                    }else{
                                ?>
                                    <span class="help-block text-right">Password is required</span>
                                <?php
                                    }
                                ?>   
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <input type="password" placeholder="Repeat password" class="form-control border-slate" name="confrm_password">
                                        <div class="form-control-feedback">
                                            <i class="icon-user-lock text-muted"></i>
                                        </div>
                                <?php
                                    if(isset($_SESSION['confirmPassword_mismatch']) || isset($_SESSION['confrmPassword_required'])){
                                ?>
                                    <label class="validation-error-label text-left">
                                <?php 
                                    $objAddUser->session_message('confirmPassword_mismatch');
                                    $objAddUser->session_message('confrmPassword_required');
                                ?>
                                    <br></label>

                                <?php
                                    }else{
                                ?>
                                    <span class="help-block text-right">Password must be matched</span>
                                <?php
                                    }
                                ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group has-feedback">
                                        <input type="email" placeholder="Your email" class="form-control border-slate"  name="email" value="<?php 
                                        $objAddUser->session_message('email');
                                        ?>">
                                        <div class="form-control-feedback">
                                            <i class="icon-mention text-muted"></i>
                                        </div>
                                <?php
                                    if(isset($_SESSION['email_exists']) || isset($_SESSION['email_formateInvalid']) ||isset($_SESSION['emai_required']) ){
                                ?>
                                    <label class="validation-error-label text-left">
                                <?php 
                                    $objAddUser->session_message('email_exists');
                                    $objAddUser->session_message('email_formateInvalid');
                                    $objAddUser->session_message('emai_required');
                                ?>
                                    <br></label>

                                <?php
                                    }else{
                                ?>
                                    <span class="help-block text-right">Email is required</span>
                                <?php
                                    }
                                ?> 
                                    </div>
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <input type="file" class="file-input bg-slate" name="image">
                                <?php
                                    if(isset($_SESSION['ErrorImageExtension']) || isset($_SESSION['ErrorImageSize'])){
                                ?>
                                    <label class="validation-error-label text-left">
                                <?php 
                                    $objAddUser->session_message('ErrorImageExtension');
                                    $objAddUser->session_message('ErrorImageSize');
                                ?>
                                    <br></label>

                                <?php
                                    }elseif(isset($_SESSION['NeedImage'])){
                                ?>  
                                    <label class="validation-error-label text-success text-left">
                                <?php
                                        $objAddUser->session_message('NeedImage');
                                ?>
                                    <br></label>
                                <?php

                                    }else{
                                ?>
                                    <span class="help-block text-right">Additional photo is Optional</span>
                                <?php
                                    }
                                ?>
                            </div>
                            <div class="text-left">
                                <label class="radio-inline">
                                    <input type="radio" class="styled" name="is_admin" value="1" checked="checked">As Admin
                                </label>

                                <label class="radio-inline">
                                            <input type="radio" class="styled" name="is_admin" value="2">
                                            As user
                                </label>
                            </div>
                        </div>
                        <div class="text-right">
                            <button class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10" type="submit"><b><i class=" icon-arrow-right15"></i></b> Create account</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    
    <!-- /grid -->
    
    <script type="text/javascript" src="../assets/js/pages/uploader_bootstrap.js"></script>

    <script type="text/javascript" src="../assets/js/plugins/uploaders/fileinput.min.js"></script>
    <?php include_once 'footer.php' ?>