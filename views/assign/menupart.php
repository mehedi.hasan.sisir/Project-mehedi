           
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><span class="text-semibold">Assign</span> New Session <i class="icon-arrow-right14 position-left"></i></h4>
                    </div>
                    <?php $objAssignNew->session_message('assignStoreSuccess');?>
                </div>
            </div>
            <!-- /page header -->

<div class="panel-flat">
    <!-- Grid -->
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel registration-form">
                <div class="panel-body">
            <!-- Content area -->
            <div class="content">
                <!-- deatched content -->
                <div class="container-detached">
                    <div class="content-detached">
                        <form class="form-horizontal" action="add_assign_action.php" method="POST" class="assign">
                            <div class="panel panel-flat">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <fieldset>
