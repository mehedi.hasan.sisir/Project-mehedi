<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\software\software;

$objLoginUser = new user_login();
$objLoginUser -> login_check();

$objAllsoftwares = new software();

$allsoftwareInfo = $objAllsoftwares -> ShowAllsoftware();

$i = 1;
$serial = 1; 


include_once '../header.php';
include_once 'menubar.php';

$objAllsoftwares -> session_message('softwareDisabledSuccess');
$objAllsoftwares -> session_message('ReinstalledSuccess');
?>
<div class="panel panel-flat">
	<div class="panel-heading">
		<h3 class="panel-title text-center">All software info</h4>
	</div>
	<div class="panel-body">
		<table class="table datatable-basic datatable-responsive">
			<thead>
				<tr class="bg-info-800">
					<th class="col-md-1">#</th>
					<th class="col-md-2">Software Name</th>
					<th class="col-md-2">Software Version</th>
					<th class="col-md-2">Software type</th>
					<th class="col-md-2">Assigned Lab</th>
					<th class="col-md-2 text-center">Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				if(isset($allsoftwareInfo) && !empty($allsoftwareInfo)){

					foreach ($allsoftwareInfo as $key => $singlesoftwareInfo) {
				
				?>
				<tr class="<?php 
					echo ($i % 2 == 0)?'info':'alpha-grey';
					 $i++; ?>">

            		<td><?php echo $serial; $serial++; ?></td>
					<td class="text-center">
					<?php echo $singlesoftwareInfo['software_title']; 
					?>
					</td>
					<td>Version <?php echo $singlesoftwareInfo['version']; ?></td>
					<td><?php echo $singlesoftwareInfo['software_type'];?>
					</td>

					<td>Lab Number <?php echo $singlesoftwareInfo['lab_no']; ?></td>
					<td class = "text-center">
						<div class="btn-group">
									<a href="edit_software.php?id=<?php echo $singlesoftwareInfo['id'];?>"class="btn bg-teal btn-xs" type="button"><i class="icon-pencil7 position-left"></i> Edit
									</a>
									<?php 
				                        if($_SESSION['logged']['is_admin'] == 1){
				                    ?>
									<a href="delete_software.php?id=<?php echo $singlesoftwareInfo['id'];?>" class="btn bg-teal btn-xs" type="button" onclick = "return confirm('Are you sure to uninstall this software?')"><i class="icon-close2 position-left"></i>Uninstall
									</a>
									<?php 
										}
									?>
								</div>
					</td>
				</tr>
				<?php
					}// foreach
				}// if $allsoftwareInfo not empty
				?>
			</tbody>
		</table>
	</div>
</div>
<!-- /basic datatable -->
<script type="text/javascript" src="../assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script type="text/javascript" src="../assets/js/pages/datatables_basic.js"></script>
<?php include_once 'footer.php' ?>