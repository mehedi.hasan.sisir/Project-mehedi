<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\courses\courses;
$objLoginUser = new user_login();
$objLoginUser -> login_check();

$objSingleCourse = new courses();
$objSingleCourse->prepare($_GET);
$singleCourses = $objSingleCourse->single_courses();

if (!empty( $singleCourses) && isset( $singleCourses)) {
	include_once '../header.php';
	include_once 'menubar.php';
?>
<div class="row">
<div class="col-md-12">
	<!-- Horizontal form -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h3 class="panel-title">Course Description </h3>
			<div class="heading-elements">
				<div class="heading-btn-group">
					<a type="button" class="btn bg-teal btn-labeled" href="edit_course.php?id=<?php echo $singleCourses['unique_id'];?>"><b><i class="icon-pencil7"></i></b> Edit Course</a>
					<?php
                           if($_SESSION['logged']['is_admin'] == 1){
                   	?>
					<a href="delete_courses.php?id=<?php echo $singleCourses['unique_id'];?>" class="btn bg-teal btn-labeled" onclick="return confirm('Are you sure you want to disable this course?');"><b><i class="icon-close2 position-left"></i></b>
						Disable Course
					</a>
					<?php
						}
					?>
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-bordered">
					<tbody>
						<tr class="alpha-grey">
							<th class = "col-md-3"><b>ID</b></th>
							<td class = "col-md-9"><?php echo $singleCourses['id']; ?></td>
						</tr>
						<tr class="alpha-slate">
							<th class = "col-md-3"><b>Course Name</b></th>
							<td class = "col-md-9"><?php echo $singleCourses['title']; ?></td>
						</tr>
						<tr class="alpha-grey">
							<th class = "col-md-3"><b>Duration</b></th>
							<td class = "col-md-9">
								<?php
								if ($singleCourses['duration'] == '15_days') {
								echo "15 Days";
								}
								if ($singleCourses['duration'] == '1_month') {
								echo "1 Month";
								}
								if ($singleCourses['duration'] == '2_months') {
								echo "2 Months";
								}
								if ($singleCourses['duration'] == '3_months') {
								echo "3 Months";
								}
							?></td>
						</tr>
						<tr class="alpha-slate">
							<th class = "col-md-3"><b>Type</b></th>
							<td class = "col-md-9">
								<?php
								if ($singleCourses['course_type'] == 0) {
								echo '<b><span class="label label-flat border-danger text-danger-600"><b>FREE Course</b></span></b>';
								} else {
								echo 'Paid Course';
								}
								?>
							</td>
						</tr>
						<tr class="alpha-grey">
							<th class = "col-md-3"><b>Course Fee</b></th>
							<td class = "col-md-9">
								<?php
								if ($singleCourses['course_fee'] == 0) {
								echo'Not Applicable';
								} else {
								echo $singleCourses['course_fee'];
								}
								?>
								<?php
								if ($singleCourses['is_offer'] == 0) {
								echo '<br><span class="badge bg-purple-600">Offered</span>';
								}
								?>
							</td>
						</tr>
						<tr class="alpha-slate">
							<th class = "col-md-3"><b>Description</b></th>
							<td class = "col-md-9"><div class="more_no">
							<?php echo $singleCourses['description']; ?></div>
						</td>
					</tr>
					<tr class="alpha-grey">
						<th class = "col-md-3"><b>Created</b></th>
						<td class = "col-md-9"><?php echo $singleCourses['created']; ?></td>
					</tr>
					<tr class="alpha-slate">
						<th class = "col-md-3"><b>Modified</b></th>
						<td class = "col-md-9">
							<?php
							if ($singleCourses['updated'] == '0000-00-00 00:00:00') {
							echo "No Modify Yet";
							} else {
							echo $singleCourses['updated'];
							}
							?>
						</td>
					</tr>
					<tr class="alpha-grey">
						<th class = "col-md-3"><b>Delete</b></th>
						<td class = "col-md-9">
							<?php
							if ($singleCourses['deleted'] == '0000-00-00 00:00:00') {
							echo "No Delete Yet";
							} else {
							echo $singleCourses['deleted'];
							}
							?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- /horizotal form -->
</div>
</div>
</div>
<?php
} else {
$_SESSION['errorMsg'] = "Sorry Wrong Url";
header('location:error.php');
}
?>